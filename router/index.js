const auth = require("./auth");
const user = require("./user");
const product = require("./product");

module.exports = {
  authRoutes: auth,
  userRoutes: user,
  productRoutes: product,
};
