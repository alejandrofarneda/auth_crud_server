const express = require("express");
const multiparty = require("connect-multiparty");
const { ProductController } = require("../controllers");
const { md_auth } = require("../middlewares");

const md_upload = multiparty({ uploadDir: "./uploads/images" });
const api = express.Router();

api.get("/product/:id", [md_auth.asureAuth], ProductController.getProduct);

api.get("/products", [md_auth.asureAuth], ProductController.getProducts);

api.post(
  "/product",
  [md_auth.asureAuth, md_upload],
  ProductController.createProduct
);

api.patch(
  "/product/:id",
  [md_auth.asureAuth, md_upload],
  ProductController.updateProduct
);

api.delete(
  "/product/:id",
  [md_auth.asureAuth],
  ProductController.deleteProduct
);

module.exports = api;
