# AUTH CRUD SERVER

## Server for usage with several apps as ReactNativeCrud which you can find in:

https://gitlab.com/alejandrofarneda/reactnativecrud

» Mongoose

» Express

» Bcryptjs

» Nodemon

» Joi

» JsonWebToken

» More...

## Getting started

In the project directory, you can run:

`npm run dev`

or

`yarn dev`

The server will reload when you make changes. You may also see any lint errors in
the console.

## Check .env.example

## Coming up next:

» Testing

» Typescript
