const { jwt } = require("../utils")
const { helpers } = require("../utils")

function asureAuth(req, res, next) {
  const { makeErrorResponse } = helpers
  const authorization = req.headers.authorization

  if (!authorization) {
    makeErrorResponse(res, 403, "Must have an authorization header")
  }

  const token = authorization.replace("Bearer ", "")

  try {
    const payload = jwt.decoded(token)

    const { exp } = payload
    const currentData = new Date().getTime()

    if (exp <= currentData) {
      return makeErrorResponse(res, 400, "Token expired")
    }

    req.user = payload

    next()
  } catch (error) {
    makeErrorResponse(res, 400, "Token invalid")
  }
}

module.exports = {
  asureAuth,
}
