const { UPLOADS_FOLDER } = require("../constants");
const fs = require("fs");

function random_bg_color() {
  var x = Math.floor(Math.random() * 180);
  var y = Math.floor(Math.random() * 180);
  var z = Math.floor(Math.random() * 180);
  return "rgb(" + x + "," + y + "," + z + ")";
}

function getFilePath(file) {
  const filePath = file.path;
  const fileSplit = filePath.split("/");

  const resultFilePath = `${fileSplit[1]}/${fileSplit[2]}`;

  return resultFilePath;
}

function makeErrorResponse(res, code, message) {
  res.status(code).send({ msg: message });
}

function deleteLocalImage(imagePath) {
  fs.unlink(`${UPLOADS_FOLDER}${imagePath}`, (err) => {
    if (err) {
      console.log("Error deleting avatar");
    }
  });
}

module.exports = {
  random_bg_color,
  getFilePath,
  makeErrorResponse,
  deleteLocalImage,
};
