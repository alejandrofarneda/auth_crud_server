const express = require("express");
const bodyParser = require("body-parser");
var path = require("path");
const cors = require("cors");

const { API_VERSION } = require("./constants");

const app = express();

//import routing
const { authRoutes, userRoutes, productRoutes } = require("./router");

//Configure body parse
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Configure static folder
app.use(express.static(path.join(__dirname, "uploads")));
app.use("/images", express.static(path.join(__dirname, "uploads")));

//Configure Header HTTP - CORS
app.use(cors());

//Configure Routings
app.use(`/api/${API_VERSION}`, authRoutes);
app.use(`/api/${API_VERSION}`, userRoutes);
app.use(`/api/${API_VERSION}`, productRoutes);

app.use((err, req, res, next) => {
  res.status(404).send({
    error: err.message || "not found",
  });
});

module.exports = app;
