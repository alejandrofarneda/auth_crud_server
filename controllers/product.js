const { Product } = require("../models");
const { helpers } = require("../utils");

const { makeErrorResponse, deleteLocalImage } = helpers;

async function getProduct(req, res, next) {
  try {
    const { id } = req.params;

    const response = await Product.findById(id);

    if (!response) {
      makeErrorResponse(res, 400, "Product not found");
    }

    res.status(200).send(response);
  } catch (error) {
    next(error);
  }
}

async function getProducts(req, res, next) {
  try {
    const { active } = req.query;
    let response = null;
    if (active === undefined) {
      response = await Product.find();
    } else {
      response = await Product.find({ active });
    }

    res.status(200).send(response);
  } catch (error) {
    next(error);
  }
}

async function createProduct(req, res, next) {
  try {
    const product = new Product({ ...req.body });

    const image = req.files.image;

    if (image) {
      const imagePath = helpers.getFilePath(image);
      product.image = imagePath;
    }

    product.save((error, productStore) => {
      if (error) {
        makeErrorResponse(res, 400, "Error saving product");
      } else {
        res.status(201).send(productStore);
      }
    });
  } catch (error) {
    next(error);
  }
}

async function updateProduct(req, res, next) {
  try {
    const { id } = req.params;
    const productData = req.body;
    const image = req?.files?.image || null;

    if (image) {
      const imagePath = helpers.getFilePath(image);
      productData.image = imagePath;
    }

    Product.findByIdAndUpdate({ _id: id }, productData, (error) => {
      if (error) {
        makeErrorResponse(res, 400, "Error saving product");
      } else {
        res.status(200).send({ msg: "Product updated" });
        console.log(productData);
      }
    });
  } catch (error) {
    next(error);
  }
}

async function deleteProduct(req, res, next) {
  try {
    const { id } = req.params;

    const product = await Product.findById(id);

    if (!product) {
      makeErrorResponse(res, 400, "Product not found");
      return;
    }

    Product.findByIdAndDelete(id, (error) => {
      if (error) {
        makeErrorResponse(res, 400, "Error deleting product");
        return;
      } else {
        deleteLocalImage(product.image);
        res.status(200).send({ msg: "Product deleted" });
      }
    });
  } catch (error) {
    next(error);
  }
}

module.exports = {
  getProduct,
  getProducts,
  createProduct,
  updateProduct,
  deleteProduct,
};
