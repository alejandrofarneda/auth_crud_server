const bcrypt = require("bcryptjs")
const { User } = require("../models")
const { helpers } = require("../utils")
const { deleteLocalImage } = require("../utils/helpers")

const { makeErrorResponse } = helpers

async function getMe(req, res) {
  const { user_id } = req.user

  const response = await User.findById(user_id)

  if (!response) {
    res.status(400).send({ msg: "User not found" })
  } else {
    res.status(200).send(response)
  }
}

async function getUser(req, res) {
  const { id } = req.params

  const response = await User.findById(id)

  if (!response) {
    makeErrorResponse(res, 400, "User not found")
  }

  res.status(200).send(response)
}

async function getUsers(req, res) {
  const { active } = req.query
  let response = null

  if (active === undefined) {
    response = await User.find()
  } else {
    response = await User.find({ active })
  }

  res.status(200).send(response)
}

async function createUser(req, res) {
  const { password } = req.body

  const salt = bcrypt.genSaltSync(10)
  const hashPassword = bcrypt.hashSync(password, salt)

  const user = new User({
    ...req.body,
    active: false,
    password: hashPassword,
  })

  const avatar = req.files.avatar

  if (avatar) {
    const imagePath = helpers.getFilePath(avatar)
    user.avatar = imagePath
  }

  user.save((error, userStore) => {
    if (error) {
      makeErrorResponse(res, 400, "Error saving user")
    } else {
      res.status(201).send(userStore)
    }
  })
}

async function updateUser(req, res) {
  const { id } = req.params
  const userData = req.body

  const newPassword = userData.password

  if (newPassword) {
    const salt = bcrypt.genSaltSync(10)
    const hashPassword = bcrypt.hashSync(newPassword, salt)

    userData.password = hashPassword
  } else {
    delete userData.password
  }

  const avatar = req.files.avatar

  if (avatar) {
    const imagePath = helpers.getFilePath(avatar)
    userData.avatar = imagePath
  }

  User.findByIdAndUpdate({ _id: id }, userData, (error) => {
    if (error) {
      makeErrorResponse(res, 400, "Error saving user")
    } else {
      res.status(200).send({ msg: "User updated" })
    }
  })
}

async function deleteUser(req, res) {
  const { id } = req.params

  const user = await User.findById(id)

  if (!user) {
    makeErrorResponse(res, 400, "User not found")
    return
  }

  User.findByIdAndDelete(id, (error) => {
    if (error) {
      makeErrorResponse(res, 400, "Error deleting user")
    } else {
      deleteLocalImage(user.avatar)

      res.status(200).send({ msg: "User deleted" })
    }
  })
}

module.exports = {
  getMe,
  getUser,
  getUsers,
  createUser,
  updateUser,
  deleteUser,
}
