const auth = require("./auth");
const user = require("./user");
const product = require("./product");

module.exports = {
  AuthController: auth,
  UserController: user,
  ProductController: product,
};
