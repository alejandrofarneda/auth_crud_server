const Joi = require("joi")
const bcrypt = require("bcryptjs")
const { User } = require("../models")
const { jwt, helpers } = require("../utils")

const { makeErrorResponse, random_bg_color } = helpers

async function register(req, res) {
  try {
    const { email, password } = req.body

    if (!email) makeErrorResponse(res, 400, "Must have email")
    if (!password) makeErrorResponse(res, 400, "Must have password")

    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(20).required(),
    })

    await schema.validateAsync({
      email,
      password,
    })

    const emailLowerCase = email.toLowerCase()

    User.findOne({ email: emailLowerCase }, (error, userStore) => {
      if (error) {
        makeErrorResponse(res, 500, "Server error")
      }

      if (userStore?.email === email)
        makeErrorResponse(res, 409, "User all ready exists")
    })

    const user = new User({
      firstname: "",
      lastname: "",
      password,
      email: email.toLowerCase(),
      role: "user",
      color: random_bg_color().toString(),
      active: true,
    })

    const salt = bcrypt.genSaltSync(10)
    const hashPassword = bcrypt.hashSync(password, salt)

    user.password = hashPassword

    user.save((error, userStorage) => {
      if (error) {
        makeErrorResponse(res, 400, "User registration error")
      } else {
        res.status(200).send(userStorage)
      }
    })
  } catch (error) {
    throw error
  }
}

function login(req, res) {
  try {
    const { email, password } = req.body

    if (!email) makeErrorResponse(res, 400, "Must have email")
    if (!password) makeErrorResponse(res, 400, "Must have password")

    const emailLowerCase = email.toLowerCase()

    User.findOne({ _email: emailLowerCase }, (error, userStore) => {
      if (error) {
        makeErrorResponse(res, 500, "Server error")
      } else {
        bcrypt.compare(password, userStore.password, (bcryptError, check) => {
          if (bcryptError) {
            makeErrorResponse(res, 500, "Server error")
          } else if (!userStore.active) {
            makeErrorResponse(res, 401, "Unauthorized user")
          } else if (!check) {
            makeErrorResponse(res, 400, "Username or password incorrect")
          } else {
            res.status(200).send({
              access: jwt.createAccessToken(userStore),
              refresh: jwt.createRefreshToken(userStore),
            })
          }
        })
      }
    })
  } catch (error) {
    throw error
  }
}

function refreshAccessToken(req, res) {
  try {
    const { token } = req.body

    if (!token) makeErrorResponse(res, 400, "Must have token")

    const { user_id } = jwt.decoded(token)

    User.findOne({ _id: user_id }, (error, userStorage) => {
      if (error) {
        makeErrorResponse(res, 500, "Server error")
      }

      res.status(200).send({
        accessToken: jwt.createAccessToken(userStorage),
      })
    })
  } catch (error) {
    throw error
  }
}

module.exports = {
  register,
  login,
  refreshAccessToken,
}
